env                          = "devtest"
keyvault_name                = "rosetta-dev"
keyvault_resource_group      = "rosetta-dev"
client_sp_id_secret_name     = "rosetta-dev-eastus-sp-id"
client_sp_secret_secret_name = "rosetta-dev-eastus-sp-secret"

main_location                     = "eastus2"
aks_dns_prefix                    = "rosetta-devtest"
aks_kubernetes_version            = "1.19.11"
ssh_key_file                      = "dev.pub"
dns_zone_prefix                   = "devtest.dev"
aks_default_nodepool_count        = 3
container_registry_name           = "rosettadev"
container_registry_resource_group = "rosetta-dev"
add_aks_sp_to_acr                 = false