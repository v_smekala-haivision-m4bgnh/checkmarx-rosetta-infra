provider "azurerm" {
  features {}
}

terraform {
  required_version = ">=1.0.1"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.72.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "rosetta-common-inf"
    storage_account_name = "rosettaterraformstate"
    container_name       = "tfstatefiles"
    key                  = "rosetta-infra.tfstate"
  }
}
