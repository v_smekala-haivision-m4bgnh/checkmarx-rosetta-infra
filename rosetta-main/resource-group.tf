resource "azurerm_resource_group" "rg_rosetta_dev" {
  name     = "rosetta-${var.env}"
  location = var.main_location
  tags     = var.main_tags
}
