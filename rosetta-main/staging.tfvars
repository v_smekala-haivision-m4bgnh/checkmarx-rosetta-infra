env                          = "staging"
main_location                = "eastus"
keyvault_name                = "rosetta-dev"
keyvault_resource_group      = "rosetta-dev"
client_sp_id_secret_name     = "rosetta-staging-eastus-sp-id"
client_sp_secret_secret_name = "rosetta-staging-eastus-sp-secret"

aks_dns_prefix                    = "rosetta-staging-6c25ec9a-2619"
aks_kubernetes_version            = "1.19.11"
ssh_key_file                      = "staging.pub"
dns_zone_prefix                   = "staging"
aks_default_nodepool_count        = 4
container_registry_name           = "rosettadev"
container_registry_resource_group = "rosetta-dev"
add_aks_sp_to_acr                 = true
