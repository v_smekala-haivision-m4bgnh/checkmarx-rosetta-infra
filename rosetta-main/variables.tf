# main vars
variable "env" {
  description = "Environment name"
  type        = string
}

variable "main_location" {
  description = "Short name of the main resources region"
}

variable "main_tags" {
  description = "Map of common tags for all resources"
  type        = object({})
}

variable "dns_zone_name" {
  description = "DNS Zone Name"
  type        = string
}

variable "keyvault_name" {
  description = "The name of the keyavult which contains all required secret"
  type        = string
}

variable "keyvault_resource_group" {
  description = "The name of the resource group where keyavult with secrets is located"
  type        = string
}

variable "client_sp_id_secret_name" {
  description = "The name of the keyvault secret with client id"
  type        = string
}

variable "client_sp_secret_secret_name" {
  description = "The name of the keyvault secret with client secret"
  type        = string
}

# variable "ems_a_records" {
#   description = "(optional) List of DNS A records"
#   type = set(object({
#     name = string,
#     ip   = string,
#   }))
#   default = []
# }

variable "aks_kubernetes_version" {
  type = string
}

variable "aks_default_nodepool_name" {
  type = string
}

variable "aks_default_nodepool_vm_size" {
  type = string
}

variable "aks_default_nodepool_count" {
  type = string
}

variable "aks_linux_admin_username" {
  type = string
}

variable "aks_dns_prefix" {
  type = string
}

variable "ssh_key_file" {
  description = "Name of the file with ssh public key"
  type        = string
}

variable "dns_zone_prefix" {
  description = "The prefix of DNS zone. E.g. dev or example.dev"
  type        = string
}

variable "container_registry_name" {
  description = "The name of Azure Container Registry"
  type        = string
}

variable "container_registry_resource_group" {
  description = "The name of resource group where Azure Container Registry is located"
  type        = string
}

variable "add_aks_sp_to_acr" {
  description = "Create an AcrPool policy on ACR for AKS service principal."
  type        = bool
}
