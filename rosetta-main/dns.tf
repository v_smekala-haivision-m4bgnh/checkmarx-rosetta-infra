
resource "azurerm_dns_zone" "dns_child" {
  name                = "${var.dns_zone_prefix}.${var.dns_zone_name}"
  resource_group_name = azurerm_resource_group.rg_rosetta_dev.name
  tags                = var.main_tags
}
