# Resource group
main_tags = {}

dns_zone_name = "ems.haivision.com"

# ems_a_records = [
#   { name = "dev", ip = "52.224.88.35" },
#   { name = "dev-id", ip = "52.224.88.35" },
#   { name = "dev-mqtt", ip = "52.146.62.31" },
#   { name = "dev-ops", ip = "52.191.33.134" },
#   { name = "jkilada", ip = "52.224.88.35" },
#   { name = "jkilada-mqtt", ip = "52.191.224.128" },
#   { name = "josh", ip = "52.224.88.35" },
#   { name = "josh-mqtt", ip = "52.191.37.45" },
#   { name = "josh-test", ip = "52.224.88.35" },
#   { name = "josh-test-mqtt", ip = "20.42.36.198" },
#   { name = "keycloak", ip = "51.143.126.7" },
#   { name = "philip", ip = "52.224.88.35" },
#   { name = "philip-mqtt", ip = "52.224.28.11" },
# ]

# AKS
aks_default_nodepool_name    = "systemv4"
aks_default_nodepool_vm_size = "Standard_D8s_v4"
aks_linux_admin_username     = "azureuser"
