env                          = "dev"
main_location                = "eastus"
keyvault_name                = "rosetta-dev"
keyvault_resource_group      = "rosetta-dev"
client_sp_id_secret_name     = "rosetta-dev-eastus-sp-id"
client_sp_secret_secret_name = "rosetta-dev-eastus-sp-secret"

aks_dns_prefix                    = "rosetta-de-rosetta-dev-f02bd9"
aks_kubernetes_version            = "1.19.7"
ssh_key_file                      = "dev.pub"
dns_zone_prefix                   = "dev"
aks_default_nodepool_count        = 4
container_registry_name           = "rosettadev"
container_registry_resource_group = "rosetta-dev"
add_aks_sp_to_acr                 = true
