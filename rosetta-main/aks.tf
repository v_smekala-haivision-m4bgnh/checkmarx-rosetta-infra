# This keyvault has to be created in advance in a different resource group
# as we need the secrets value from it.
data "azurerm_key_vault" "keyvault" {
  name                = var.keyvault_name # output from the module that creates the Key Vault
  resource_group_name = var.keyvault_resource_group
}

data "azurerm_key_vault_secret" "aks_sp_client_id" {
  name         = var.client_sp_id_secret_name
  key_vault_id = data.azurerm_key_vault.keyvault.id
}

data "azurerm_key_vault_secret" "aks_sp_client_secret" {
  name         = var.client_sp_secret_secret_name
  key_vault_id = data.azurerm_key_vault.keyvault.id
}

resource "azurerm_kubernetes_cluster" "aks_rosetta_dev" {
  name                       = "rosetta-${var.env}-${var.main_location}"
  location                   = azurerm_resource_group.rg_rosetta_dev.location
  resource_group_name        = azurerm_resource_group.rg_rosetta_dev.name
  dns_prefix                 = var.aks_dns_prefix
  kubernetes_version         = var.aks_kubernetes_version
  node_resource_group        = "MC_rosetta-${var.env}_rosetta-${var.env}-${var.main_location}_${var.main_location}"
  enable_pod_security_policy = false

  service_principal {
    client_id     = data.azurerm_key_vault_secret.aks_sp_client_id.value
    client_secret = data.azurerm_key_vault_secret.aks_sp_client_secret.value
  }

  default_node_pool {
    name               = var.aks_default_nodepool_name
    type               = "VirtualMachineScaleSets"
    availability_zones = [1, 2, 3]
    vm_size            = var.aks_default_nodepool_vm_size

    enable_node_public_ip = false
    max_pods              = 250

    enable_auto_scaling = true
    max_count           = 5
    min_count           = 3
    node_count          = var.aks_default_nodepool_count
  }

  linux_profile {
    admin_username = var.aks_linux_admin_username

    # using the value from keyvault would cause a force replacement
    ssh_key {
      key_data = file("public_keys/${var.ssh_key_file}")
    }
  }

  tags = var.main_tags
}

data "azurerm_container_registry" "acr" {
  name                = var.container_registry_name
  resource_group_name = var.container_registry_resource_group
}

data "azuread_service_principal" "aks_principal" {
  application_id = data.azurerm_key_vault_secret.aks_sp_client_id.value
}

resource "azurerm_role_assignment" "acrpull_role" {
  count = var.add_aks_sp_to_acr ? 1 : 0

  scope                            = data.azurerm_container_registry.acr.id
  role_definition_name             = "AcrPull"
  principal_id                     = data.azuread_service_principal.aks_principal.id
  skip_service_principal_aad_check = true
}
