resource "azurerm_storage_account" "storagedev" {
  name                     = "rosetta${var.env}"
  location                 = azurerm_resource_group.rg_rosetta_dev.location
  resource_group_name      = azurerm_resource_group.rg_rosetta_dev.name
  account_tier             = "Standard"
  account_replication_type = "RAGRS"
  allow_blob_public_access = true
  min_tls_version          = "TLS1_2"
  is_hns_enabled           = false

  tags = var.main_tags
}

resource "azurerm_storage_account" "storagedevfiles" {
  name                     = "rosetta${var.env}files"
  location                 = azurerm_resource_group.rg_rosetta_dev.location
  resource_group_name      = azurerm_resource_group.rg_rosetta_dev.name
  account_tier             = "Standard"
  account_replication_type = "RAGRS"
  allow_blob_public_access = false
  min_tls_version          = "TLS1_2"
  is_hns_enabled           = true

  tags = var.main_tags
}
