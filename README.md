# Overview

Terraform files to manage rosetta infrastructure. At first targetting rosetta-dev. Later (likely using terraform workspaces) the same repo will be used to deploy all environments.

Unlike original Rosetta infrastructure project, which was dependend on preexisting Service Principal, a KeyVault (with SPN's credentials in it) and a Container Registry, current terraform files will create these on demand.

Deployment files are organized into 2 folder:

  - `rosetta-core`, which deploys bootstrap resources such as Service Principal, a KeyVault and KeyVault access policies
  - `rosetta-main`, which deploys rosetta infra resources, such as AKS, Storage Account, etc.

There's no strict need to keep aforementioned resources in separate folders, since Terraform is good in building a proper dependencies tree. However, since Non-bespin environment relies on SPN and KV to be creted beforehand, the folders can be kept in order to make deployment with or without `rosetta-core` contents easier.

## Dependenices

* [terraform](https://www.terraform.io/downloads.html)
* [azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)

## Azure Naming and Tagging strategy

https://docs.microsoft.com/en-us/azure/cloud-adoption-framework/ready/azure-best-practices/naming-and-tagging

## Workflow

Variable definition is splitted into 2 files: one common file with variables, which are common between all the environments (config.auto.tfvars) and second is specific per environment (dev.tfvars, qa.tfvars, etc.).

```bash
az login
# or
az login --service-principal -t ${TENANT_ID} -u ${CLIENT_ID} -p ${CLIENT_SECRET}
az account set --subscription="SUB_ID"

cd rosetta-dev

terraform init
terraform fmt .
terraform plan -var-file=dev.tfvars
terraform apply -var-file=dev.tfvars
```

Deploying new environment: run `terraform workspace new qa2` or `terraform workspace select qa2`. Then run `terraform apply -var-file=qa2.tfvars`

## Creating a new environment

### Prerequisites

* create new ssh public key and place it into public\_keys folder
* create new tfvars file and populate it accordingly to your new environemnt
* run `terraform workspace new workspace-name-here` command
* run `terraform apply -var-file=env-name.tfvars`
* you may need to add a DNS A record which points to the newly crated DNS
