variable "tenant_id" {
  type = string
}

variable "subscription_id" {
  type = string
}

variable "acr_name" {
  type = string
}

variable "main_tags" {
  type = map(string)
}

variable "location" {
  type    = string
  default = "eastus"
}

variable "resource_group_name" {
  type = string
}
