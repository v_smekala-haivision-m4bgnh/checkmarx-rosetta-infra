terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.81.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "= 2.6.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "rosetta-core-rg"      # var.resource_group_name
    storage_account_name = "terraformstate693048" # azurerm_storage_account.state.name
    container_name       = "terraform-state"      # azurerm_storage_container.state-container.name
    key                  = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {}

  subscription_id = var.subscription_id
  tenant_id       = var.tenant_id
}

data "azuread_client_config" "current" {}
data "azurerm_subscription" "current" {}


resource "azuread_application" "rosetta_core" {
  display_name = "Rosetta Core Automation"
  owners       = [data.azuread_client_config.current.object_id]
}

resource "azuread_application" "rosetta_acr" {
  display_name = "Rosetta ACR"
  owners       = [data.azuread_client_config.current.object_id]
}

resource "azuread_service_principal" "rosetta" {
  application_id               = azuread_application.rosetta_core.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]
}

resource "azuread_service_principal" "acr_pull_sp" {
  application_id               = azuread_application.rosetta_acr.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]
}

resource "azuread_service_principal_password" "this" {
  service_principal_id = azuread_service_principal.rosetta.object_id
}

resource "azuread_service_principal_password" "acr_pull_sp" {
  service_principal_id = azuread_service_principal.acr_pull_sp.object_id
}

resource "azurerm_resource_group" "this" {
  name     = var.resource_group_name
  location = var.location
  tags     = {}
}

resource "random_integer" "sa_num" {
  min = 100000
  max = 999999
}

resource "azurerm_storage_account" "state" {
  name                     = "terraformstate${random_integer.sa_num.result}"
  resource_group_name      = azurerm_resource_group.this.name
  location                 = azurerm_resource_group.this.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "state-container" {
  name                 = "terraform-state"
  storage_account_name = azurerm_storage_account.state.name
}

resource "azurerm_key_vault" "this" {
  name                        = "rosetta-kv"
  location                    = azurerm_resource_group.this.location
  resource_group_name         = azurerm_resource_group.this.name
  enabled_for_disk_encryption = true
  tenant_id                   = var.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false
  tags                        = {}
  sku_name                    = "standard"
}

resource "azurerm_key_vault_access_policy" "owner" {
  key_vault_id = azurerm_key_vault.this.id
  tenant_id    = var.tenant_id
  object_id    = data.azuread_client_config.current.object_id

  secret_permissions = [
    "Get",
    "List",
    "Set",
    "Delete",
    "Backup",
    "Restore",
    "Recover",
    "Purge",
  ]
}

resource "azurerm_key_vault_access_policy" "sp" {
  key_vault_id = azurerm_key_vault.this.id
  tenant_id    = var.tenant_id
  object_id    = azuread_service_principal.rosetta.object_id

  secret_permissions = [
    "Get",
    "List",
    "Set",
  ]
}

resource "azurerm_key_vault_access_policy" "victor" {
  key_vault_id = azurerm_key_vault.this.id
  tenant_id    = var.tenant_id
  object_id    = "59eb49af-fb14-4909-85df-1fe9a5c5b6f0"

  secret_permissions = [
    "Get",
    "List",
    "Set",
    "Delete",
    "Backup",
    "Restore",
    "Recover",
    "Purge",
  ]
}

resource "azurerm_key_vault_access_policy" "dan" {
  key_vault_id = azurerm_key_vault.this.id
  tenant_id    = var.tenant_id
  object_id    = "a838c37c-763c-4c52-835e-de41b65d7fdc"

  secret_permissions = [
    "Get",
    "List",
    "Set",
    "Delete",
    "Backup",
    "Restore",
    "Recover",
    "Purge",
  ]
}

resource "azurerm_key_vault_access_policy" "jkilada" {
  key_vault_id = azurerm_key_vault.this.id
  tenant_id    = var.tenant_id
  object_id    = "309500fe-b5f7-4777-a0f1-4f030bc856d5"

  secret_permissions = [
    "Get",
    "List",
    "Set",
    "Delete",
    "Backup",
    "Restore",
    "Recover",
    "Purge",
  ]
}

resource "azurerm_key_vault_secret" "sp_app_id" {
  depends_on = [
    azurerm_key_vault_access_policy.owner,
  ]
  name         = "rosetta-core-bespinqa-eastus-sp-id"
  value        = azuread_application.rosetta_core.application_id
  key_vault_id = azurerm_key_vault.this.id
}

resource "azurerm_key_vault_secret" "sp_password" {
  depends_on = [
    azurerm_key_vault_access_policy.owner,
  ]
  name         = "rosetta-core-bespinqa-eastus-sp-secret"
  value        = azuread_service_principal_password.this.value
  key_vault_id = azurerm_key_vault.this.id
}

resource "azurerm_key_vault_secret" "sp_tenant_id" {
  depends_on = [
    azurerm_key_vault_access_policy.owner,
  ]
  name         = "rosetta-core-bespinqa-eastus-sp-tenant-id"
  value        = data.azuread_client_config.current.tenant_id
  key_vault_id = azurerm_key_vault.this.id
}

resource "azurerm_key_vault_secret" "acr_pull_app_id" {
  depends_on = [
    azurerm_key_vault_access_policy.owner,
  ]
  name         = "rosetta-bespinqa-eastus-sp-id"
  value        = azuread_application.rosetta_acr.application_id
  key_vault_id = azurerm_key_vault.this.id
}

resource "azurerm_key_vault_secret" "acr_pull_password" {
  depends_on = [
    azurerm_key_vault_access_policy.owner,
  ]
  name         = "rosetta-bespinqa-eastus-sp-secret"
  value        = azuread_service_principal_password.acr_pull_sp.value
  key_vault_id = azurerm_key_vault.this.id
}

resource "azurerm_role_assignment" "sp-contrib" {
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "Contributor"
  principal_id         = azuread_service_principal.rosetta.object_id
}

resource "azurerm_role_assignment" "sp-acr" {
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "AcrPush"
  principal_id         = azuread_service_principal.rosetta.object_id
}

resource "azurerm_role_assignment" "sp-dns" {
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "DNS Zone Contributor"
  principal_id         = azuread_service_principal.rosetta.object_id
}

resource "azurerm_role_assignment" "sp-aks" {
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "Azure Kubernetes Service RBAC Cluster Admin"
  principal_id         = azuread_service_principal.rosetta.object_id
}

resource "azurerm_role_assignment" "sp-sa" {
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "Storage Account Contributor"
  principal_id         = azuread_service_principal.rosetta.object_id
}

resource "azurerm_role_assignment" "sp-uaa" {
  # Scope was supposed to be a resource group. However, target RG is created
  # in the second stage of deployment, so until we figure out how to move RG
  # into here it must be a Subscription scope instead
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "User Access Administrator"
  principal_id         = azuread_service_principal.rosetta.object_id
}

resource "azuread_directory_role" "app-dev-role" {
  display_name = "Application developer"
}

resource "azuread_directory_role_member" "sp-app-dev" {
  role_object_id   = azuread_directory_role.app-dev-role.object_id
  member_object_id = azuread_service_principal.rosetta.object_id
}

resource "azurerm_container_registry" "acr_rosetta" {
  name                = var.acr_name
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location
  sku                 = "Standard"
  admin_enabled       = false
  tags                = var.main_tags
}

resource "azurerm_role_assignment" "acrpull_role" {
  scope                            = azurerm_container_registry.acr_rosetta.id
  role_definition_name             = "AcrPull"
  principal_id                     = azuread_service_principal.acr_pull_sp.id
  skip_service_principal_aad_check = false
}
